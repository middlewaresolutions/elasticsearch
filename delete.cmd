oc delete bc elasticsearch -n elastic
oc delete bc kibana -n elastic

oc delete dc elasticsearch -n elastic
oc delete services elasticsearch -n elastic
oc delete route elasticsearch -n elastic
oc delete imagestream elasticsearch -n elastic

oc delete dc logstash -n elastic
oc delete services logstash -n elastic
oc delete route logstash -n elastic
oc delete imagestream logstash -n elastic

oc delete dc kibana -n elastic
oc delete services kibana -n elastic
oc delete route kibana -n elastic
oc delete imagestream kibana -n elastic

oc delete dc metricbeat -n elastic
oc delete services metricbeat -n elastic
oc delete route metricbeat -n elastic
oc delete imagestream metricbeat -n elastic
