oc login -u developer -p developer
oc project elastic

# Stop all

oc scale dc/kibana --replicas=0
oc scale dc/logstash --replicas=0
oc scale dc/metricbeats --replicas=0
oc scale dc/elasticsearch --replicas=0
