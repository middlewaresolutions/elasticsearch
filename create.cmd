oc login -u developer -p developer
oc new-project elastic

# add service account for host path
oc create serviceaccount elastic -n elastic

oc login -u system:admin
oc project elastic
oc adm policy add-scc-to-user anyuid -z default

oc login -u developer -p developer

oc new-build . --name=elasticsearch --context-dir=elasticsearch/ --strategy=docker 
oc new-app elastic/elasticsearch
# oc start-build elasticsearch --from-dir elasticsearch/
oc expose svc/elasticsearch

oc new-build . --name=kibana --context-dir=kibana/ --strategy=docker
oc new-app elastic/kibana
# oc start-build kibana --from-dir kibana/
oc expose svc/kibana

oc new-build . --name=logstash --context-dir=logstash/ --strategy=docker
oc new-app logstash
# oc start-build logstash --from-dir logstash/
oc expose svc/logstash

oc new-build . --name=metricbeat --context-dir=metricbeat/ --strategy=docker

oc start-build elasticsearch
oc start-build kibana
oc start-build logstash
oc start-build metricbeat
