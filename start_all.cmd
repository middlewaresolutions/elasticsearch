oc login -u developer -p developer
oc project elastic

# Start all
oc scale dc/elasticsearch --replicas=1
oc scale dc/kibana --replicas=1
oc scale dc/logstash --replicas=1
oc scale dc/metricbeats --replicas=1
