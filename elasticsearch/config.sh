ELASTICHOST=elasticsearch-elastic.minishift.local

curl -XPUT 'http://$ELASTICHOST/_all/_settings?preserve_existing=true' -d '{
  "index.mapper.dynamic" : "true"
}'
